//
//  ServiceTests.swift
//  ServiceTests
//
//  Created by Dmitry Shashlov on 11/26/17.
//  Copyright © 2017 Dmitry Shashlov. All rights reserved.
//

import XCTest
import ReactiveSwift
@testable import Nimble

class ServiceTests: XCTestCase {
    
    /// Test authorization
    func testAuthorization() {
        let tokenExpectation = expectation(description: "Token authorization success")
        NimbleService.default.authorize().on(value: {
            token in
            
            guard token.characters.count > 0 else {
                XCTFail("Bad token received")
                return
            }
            
            tokenExpectation.fulfill()
        }).start()
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
}
