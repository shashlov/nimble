//
//  SurveyCell.swift
//  Nimble
//
//  Created by Dmitry Shashlov on 11/26/17.
//  Copyright © 2017 Dmitry Shashlov. All rights reserved.
//

import Foundation
import UIKit

class SurveyCell : UITableViewCell {
    
    typealias ActionHandler = () -> ()
    var takeSurveyActionHandler: ActionHandler?
    
    // MARK: Outlets
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var surveyImageView: UIImageView!
    
    // MARK: Actions
    
    @IBAction func takeSurveyAction(sender: AnyObject) {
        takeSurveyActionHandler?()
    }
}
