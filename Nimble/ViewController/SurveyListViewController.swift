//
//  SurveyListViewController.swift
//  Nimble
//
//  Created by Dmitry Shashlov on 11/26/17.
//  Copyright © 2017 Dmitry Shashlov. All rights reserved.
//

import Foundation
import UIKit
import ReactiveSwift

class SurveyListViewController : UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    /// List of surveys presented to user
    var surveys: [SurveyModel]? {
        didSet {
            tableView?.reloadData()
            
            // Scrolls to top if non-empty list of surveys assigned
            if let surveys = surveys, surveys.count > 0 {
                activityIndicator.isHidden = true
                tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
        }
    }
    
    /// Survey image view load disposables
    var imageViews = [IndexPath: UIImageView]()
    var imageDisposables = [IndexPath: ReactiveSwift.Disposable]()
    
    // MARK: Outlets
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var tableView: UITableView!
    
    // MARK: Initialization
    
    init() {
        super.init(nibName: "SurveyList", bundle: nil)
        
        title = "SURVEYS"
        
        // Navigation items
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icn_reload"),
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(reloadAction(sender:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "icn_menu"),
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(menuAction(sender:)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: View
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        // Register cell nib
        tableView?.register(UINib(nibName: "SurveyCell", bundle: nil), forCellReuseIdentifier: "SurveyCell")
        edgesForExtendedLayout = []
        
        // Slow down scrolling deceleration
        tableView.decelerationRate = 0.5
        
        // Load surveys
        reload()
    }
        
    // MARK: Survey models
    
    private func reload() {
        NimbleService.default.getSurveys().on(
            started: { [weak self] in
                self?.activityIndicator.isHidden = false
            },
            value: {
                [weak self] surveys in
                self?.surveys = surveys
            }).start()
    }
    
    // MARK: Actions
    
    @IBAction func reloadAction(sender: AnyObject) {
        reload()
    }
    
    @IBAction func menuAction(sender: AnyObject) {
        // Nothing here by design
    }
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return surveys?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let surveyViewModel = SurveyViewModel(survey: surveys![indexPath.row])
        let cell = tableView.dequeueReusableCell(withIdentifier: "SurveyCell") as? SurveyCell
        cell?.backgroundColor = UIColor.clear
        cell?.titleLabel.text = surveyViewModel.survey.title
        cell?.descriptionLabel.text = surveyViewModel.survey.shortDescription
        cell?.surveyImageView.image = nil
        cell?.takeSurveyActionHandler = { [unowned self] in self.takeSurvey(survey: surveyViewModel.survey) }
        imageViews[indexPath] = cell?.surveyImageView
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height
    }
    
    // MARK: Image loading
    
    /// Start image loading when presenting cell
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let surveyViewModel = SurveyViewModel(survey: surveys![indexPath.row])
        let sp = surveyViewModel.imageSignal().on(value: { [weak self] img in
            self?.imageViews[indexPath]?.image = img
        })
        imageDisposables[indexPath] = sp.start()
    }

    /// Cancel image loading when end displaying cell
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        imageDisposables[indexPath]?.dispose()
        imageDisposables[indexPath] = nil
        imageViews[indexPath] = nil
    }
    
    // MARK: Survey actions
    
    // Default implementation does nothing
    private func takeSurvey(survey: SurveyModel) {
        navigationController?.pushViewController(UIViewController(), animated: true)
    }
    
    // MARK: Cell sticking
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        // Scroll to nearest cell when deceleration ended
        var offset = tableView.contentOffset
        let offsetY = tableView.contentOffset.y.truncatingRemainder(dividingBy: tableView.frame.size.height)
        switch offsetY {
            case let gte where gte > tableView.frame.size.height / 2:
                offset.y += tableView.frame.size.height - offsetY
            default:
                offset.y -= offsetY
        }
        
        tableView.setContentOffset(offset, animated: true)
    }

}
