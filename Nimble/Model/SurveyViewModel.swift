//
//  SurveyViewModel.swift
//  Nimble
//
//  Created by Dmitry Shashlov on 11/26/17.
//  Copyright © 2017 Dmitry Shashlov. All rights reserved.
//

import Foundation
import ReactiveSwift

class SurveyViewModel {
    
    var survey: SurveyModel
    
    init(survey aSurvey: SurveyModel) {
        survey = aSurvey
    }
    
    func imageSignal() -> SignalProducer<UIImage, NSError> {
        return SignalProducer {
            [weak self] observer, _ in
            
            guard let url = self?.survey.imageURL else {
                observer.send(error: NSError(domain: "com.dmitryshashlov.Nimble.imageService",
                                             code: 3,
                                             userInfo: [ NSLocalizedDescriptionKey : "No survey image url defined"]))
                return
            }
            
            DispatchQueue.global(qos: .background).async {
                do {
                    let data = try Data(contentsOf: url)
                    if let image = UIImage(data: data) {
                        DispatchQueue.main.async {
                            observer.send(value: image)
                            observer.sendCompleted()
                        }
                        return
                    }
                } catch { }
            }
        }
    }
}
