//
//  SurveyModel.swift
//  Nimble
//
//  Created by Dmitry Shashlov on 11/26/17.
//  Copyright © 2017 Dmitry Shashlov. All rights reserved.
//

import Foundation
import ObjectMapper

/// Model for survey
class SurveyModel : Mappable {
    
    /// Survey title
    internal(set) public dynamic var title: String = ""
    
    /// Short description of a survey
    internal(set) public dynamic var shortDescription: String = ""
    
    /// Survey cover image URL
    internal dynamic var imageURLString: String = ""
    public var imageURL: URL? {
        get {
            return Foundation.URL(string: "\(imageURLString)l")
        }
    }
    
    // MARK: Mappable
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        
        // Basic properties
        title <- map["title"]
        shortDescription <- map["description"]
        imageURLString <- map["cover_image_url"]
        
    }
}
