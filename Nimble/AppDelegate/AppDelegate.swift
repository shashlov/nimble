//
//  AppDelegate.swift
//  Nimble
//
//  Created by Dmitry Shashlov on 11/26/17.
//  Copyright © 2017 Dmitry Shashlov. All rights reserved.
//

import UIKit
import ReactiveSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Configure application appearance
        configureAppearance()
        
        // Perform authorization
        NimbleService.default.authorize().on(value: { [weak self] token in
            NimbleService.default.authToken = token
            self?.showSurveyController()
        }).start()
        
        return true
    }
    
    private func showSurveyController() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: SurveyListViewController())
        window?.makeKeyAndVisible()
    }
    
    private func configureAppearance() {
        let nba = UINavigationBar.appearance()
        nba.isTranslucent = true
        nba.barTintColor = UIColor.black.withAlphaComponent(0.5)
        nba.tintColor = UIColor.white
        nba.titleTextAttributes = [
            NSForegroundColorAttributeName : UIColor.white,
        ]
    }
}

