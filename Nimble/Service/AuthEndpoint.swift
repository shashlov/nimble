//
//  AuthEndpoint.swift
//  Nimble
//
//  Created by Dmitry Shashlov on 11/26/17.
//  Copyright © 2017 Dmitry Shashlov. All rights reserved.
//

import Foundation
import Alamofire
import ReactiveSwift

class AuthEndpoint : HTTPEndpoint {
    
    /// Authorize user (get token)
    internal func authorize() -> SignalProducer<String, NSError> {
        
        let params = [
            "grant_type" : "password",
            "username" : "carlos@nimbl3.com",
            "password" : "antikera"
        ]
        
        let request = manager.request(url,
                                      method: .post,
                                      parameters: params,
                                      encoding: parametersEncoding(.post),
                                      headers: nil)
        
        return SignalProducer {
            (observer, disposable) in
            request.response {
                dataResponse -> Void in
                
                // No response data
                guard let responseData = dataResponse.data else {
                    observer.send(error: EndpointError.CantAuthorize(description: "No data in server response").nsError)
                    return
                }
                
                do {
                    guard let responseObject = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject],
                        let token = responseObject["access_token"] as? String else {
                            observer.send(error: EndpointError.CantAuthorize(description: "No token data in server response").nsError)
                            return
                    }
                    
                    observer.send(value: token)
                    observer.sendCompleted()
                } catch(_) { // Can't serialize response data
                    observer.send(error: EndpointError.CantAuthorize(description: "Can't serialize response data").nsError)
                }
            }
        }
    }
}
