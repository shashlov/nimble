//
//  NimbleService.swift
//  Nimble
//
//  Created by Dmitry Shashlov on 11/26/17.
//  Copyright © 2017 Dmitry Shashlov. All rights reserved.
//

import Foundation
import Alamofire
import ReactiveSwift

class NimbleService {
    
    /// Service singleton
    public static var `default` = NimbleService(baseURL: URL(string: "https://nimbl3-survey-api.herokuapp.com/")!)
    
    // Authorization endpoint
    private var authEndpoint: AuthEndpoint
    
    // Survey endpoint
    private var surveyEndpoint: HTTPEndpoint
    
    // Auth token
    public var authToken: String? {
        didSet {
            surveyEndpoint.authToken = authToken
        }
    }
    
    // Default initializer
    public required init(baseURL: URL) {
        surveyEndpoint = HTTPEndpoint(baseUrl: baseURL, path: "surveys")
        authEndpoint = AuthEndpoint(baseUrl: baseURL, path: "oauth/token")
    }
    
    /// Get list of surveys
    public func getSurveys() -> SignalProducer<[SurveyModel], NSError> {
        return surveyEndpoint.get()
    }
    
    /// Authorize user (get token)
    public func authorize() -> SignalProducer<String, NSError> {
        return authEndpoint.authorize()
    }
}
