//
//  HTTPEndpoint.swift
//  Nimble
//
//  Created by Dmitry Shashlov on 11/26/17.
//  Copyright © 2017 Dmitry Shashlov. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import ReactiveSwift

// Foreground endpoint
open class HTTPEndpoint {
    
    // URL session manager
    internal var manager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10
        configuration.timeoutIntervalForResource = 10
        return Alamofire.SessionManager(configuration: configuration)
    }()
    
    // Base url to endpoint
    internal(set) public var baseUrl: URL
    
    // Path to endpoint
    internal(set) public var path: String?
    
    // Authorization token
    internal(set) public var authToken: String?
    
    // Full URL to endpoint
    internal var url: URL {
        if let path = path {
            return baseUrl.appendingPathComponent(path)
        }
        return baseUrl
    }
    
    public init(baseUrl aBaseUrl: URL, path aPath: String?) {
        baseUrl = aBaseUrl
        path = aPath
    }
    
    // MARK: CRUD methods
    
    public func get<T: Mappable>() -> SignalProducer<[T], NSError> {
        let request = manager.request(url,
                                      method: .get,
                                      parameters: [ "access_token" : authToken ?? "" ],
                                      encoding: parametersEncoding(.get),
                                      headers: nil)
        
        return SignalProducer {
            (observer, disposable) in
                request.responseArray { (response: DataResponse<[T]>) in
                    switch response.result {
                        
                        case .success(let objects):
                            observer.send(value: objects)
                            observer.sendCompleted()
                        
                        default:
                            observer.send(error: EndpointError.UnexpectedResponse.nsError)
                    }
                }
        }
    }
    
    // MARK: Private: Parameters encoding
    
    internal func parametersEncoding(_ method: Alamofire.HTTPMethod) -> Alamofire.ParameterEncoding {
        switch method {
            case .get: return URLEncoding.default
            default: return JSONEncoding.default
        }
    }
}
