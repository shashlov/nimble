//
//  EndpointError.swift
//  Nimble
//
//  Created by Dmitry Shashlov on 11/26/17.
//  Copyright © 2017 Dmitry Shashlov. All rights reserved.
//

import Foundation

enum EndpointError {
    
    case CantAuthorize(description: String)
    case UnexpectedResponse
    
    var nsError: NSError {
        switch self {
            
        // Can't authorize with service
        case .CantAuthorize(let description):
            return NSError(domain: "com.dmitryshashlov.Nimble.service",
                           code: 1,
                           userInfo: [ NSLocalizedDescriptionKey : "Can't authorize: \(description)"])
            
        // Unexpected response
        case .UnexpectedResponse:
            return NSError(domain: "com.dmitryshashlov.Nimble.service",
                           code: 2,
                           userInfo: [ NSLocalizedDescriptionKey : "Can't handle response"])
        }
    }
}
